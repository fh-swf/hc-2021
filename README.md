# Hash Code 2021 Practice Round

This repository contains my initial experiments on the 2021 practice problem.

## Score
My best scores so far are:
| Problem | score |
|:----------|------:|
| A – Example | 74 points |
| B – A little bit of everything | 13,564 points |
| C – Many ingredients | 692,308,193 points |
| D – Many pizzas | 5,344,565 points |
| E – Many teams | 8,240,691 points |
| **Total score** | 705,907,087 points |
